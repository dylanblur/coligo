# Coligo

<p style="text-align:center"><img src="./src/assets/cologo.svg"></p>

Coligo means link (network/connection) in latin and is a contact book web app, made solely for c-point take home test project

## Features ⚡

- Login and Register (not an actual authentication)
- Manage contacts
  - add/remove/edit/view
  - filter contacts
  - search through contacts
  - set as favorite
- Take footprint information on users
- [Admin] Get a network graph of the users
- [Admin] View statistics.

## Shortcomings 🐣

With enough time, I'd have...

- Added a Dark Mode 🌚
- Written unit test for the components
- End to End tested the app in production environment
- Added a better authentication flow
- Write more modularized code
- Optimized for mobile rendering (why not making a PWA ?)
- Added beautiful stunning animations on page transitions and on micro actions
- Made uploading pictures possible :D

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## Running Locally 🏃🏾‍♂️

Assuming you have Angular CLI installed...

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build 🔨

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests (UNDONE)

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests (UNDONE)

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Licence

This code is solely written for the C-point interview purpose. However, it cannot be redistributed or used in anyway without my explicit conscent [Dylan Tientcheu](mailto:dylantientcheu@gmail.com)
