import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  NbThemeModule,
  NbLayoutModule,
  NbCardModule,
  NbInputModule,
  NbButtonModule,
  NbTabsetModule,
  NbUserModule,
  NbIconModule,
  NbSpinnerModule,
  NbSidebarModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ContactBookComponent } from './components/contact-book/contact-book.component';
import { ContactsDataService } from './data/contacts.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomePipe } from './home/home.pipe';
import { AdminComponent } from './admin/admin.component';
import { ChartsModule } from 'ng2-charts';




const NB_MODULES = [
  NbCardModule,
  NbLayoutModule,
  NbInputModule,
  NbButtonModule,
  NbTabsetModule,
  NbUserModule,
  NbIconModule,
  NbSpinnerModule,
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ContactBookComponent,
    HomePipe,
    AdminComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ...NB_MODULES,
    NbSidebarModule.forRoot(),
    NbThemeModule.forRoot({ name: 'default' }),
    NbEvaIconsModule,
    FormsModule,
    ChartsModule,
    ReactiveFormsModule
  ],
  providers: [ContactsDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
