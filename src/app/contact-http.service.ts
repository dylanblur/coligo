import { Injectable } from '@angular/core';
import { SERVER_URL, ADMIN_LOGIN, USER_LOGIN } from '../app.constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactHttpService {

  adminLogin = ADMIN_LOGIN;
  userLogin = USER_LOGIN;

  constructor(public http: HttpClient) { }

  addContacts(body): Observable<any> {
    return this.http.post(SERVER_URL, body);
  }

  getContacts(): Observable<any> {
    return this.http.get(SERVER_URL);
  }

  deleteContact(id) {
    return this.http.delete(`${SERVER_URL}/${id}`);
  }

  updateContact(id, body) {
    return this.http.put(`${SERVER_URL}/${id}`, body);
  }
}
